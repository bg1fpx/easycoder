# EasyCoder

EasyCoder is an encoding-decoding app for your instant messenger (IM), such as WhatsApp or WeChat. It can convert a readable message to an unreadable codeblock, or vice versa.

![image](https://gitlab.com/bg1fpx/easycoder/-/raw/main/screenshot.gif)

www.italian.org.cn/app/

www.zhangyichi.com
